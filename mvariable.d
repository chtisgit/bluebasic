/*

	Classes: Variable and Parameter (extends Variable)
	
	Saves neccessary values for variables and parameters in an
	instance and provides functions to deal with them.

*/

module mvariable;
import std.string;
import mtype;
import mfunction;
import mmisc;
import main;
import mmodule;
import mformula;
import mcommand;
import midentifier;

class TempVariable : Variable{
private:
	bool used = false;
	
public:
	this(string name, string typ){
		super(name, typ, name);
	}

	/*
		When the compiler begins to use a temporary variable it shall
		mark it as used using this method.
	*/	
	void lock(){
		if(used)
			error("{FRONTEND} The frontend locked a tempvar twice. This is considered to be FATAL!", true);
		used = true;
	}
	
	/*
		When the compiler has finished using a temporary variable it shall
		mark it as unused using this method. This will allow the compiler
		to reuse currently unused temporary variables.
	*/
	void unlock(){
		if(!used)
			warning("{FRONTEND} The frontend unlocked a tempvar twice");
		used = false;
	}
	
	
	bool is_locked(){
		return used;
	}


}

class Parameter : Variable{
public:
	bool optional;
	bool byref;
	string immvalue;


	this(string name, string typ, bool optional, bool byref){
		if(name)
			super(name, typ, "_" ~ name);
		else
			super(null, typ);
		this.optional = optional;
	}
	
}

class Variable : Identifier{
public:
	
	this(string name, string typ, string realname = null){
		if(name && extrn && !(realname is null)) realname = name;
		super(name, typ, realname);
		if(this.typ is null) error("type \"" ~ typ ~ "\" (of var \"" ~ name ~ "\") does not exist");
	}
	
static:

	Variable add(bool global, string name, string typ){
		Variable v;
		debugecho("NEW variable " ~ name ~ ":" ~ typ);
		string rname = null;
		if(Module.is_name_used(name)) error("identifier \"" ~ name ~ "\" is defined (at least) twice");
		if(Function.current != Function.main || global == false) rname = "_" ~ name;
		
		v = new Variable(name, typ, rname);
		
		if(global)
			Function.current.add_global(v);
		else
			Function.current.add_local(v);
			
		return v;
	}

	bool check_name(string name){
		name = toLower(name);
		foreach(Variable v; Function.current.globals){
			if(toLower(v.name) == name) return false;
		}
		foreach(Variable v; Function.current.locals){
			if(toLower(v.name) == name) return false;
		}
		foreach(Parameter p; Function.current.param){
			if(toLower(p.name) == name) return false;
		}
		return true;
	}
	
	Variable get(string name){
		name = toLower(name);
		foreach(Variable v; Function.current.globals){
			if(toLower(v.name) == name) return v;
		}
		foreach(Variable v; Function.current.locals){
			if(toLower(v.name) == name) return v;
		}
		foreach(Parameter p; Function.current.param){
			if(toLower(p.name) == name) return p;
		}
		foreach(Variable v; Function.main.globals){
			if(toLower(v.name) == name) return v;
		}
		return null;		
	}
	
	bool estne(string name){
		return !(get(name) is null);
	}
}

void var_exdefinition(){
	string name;
	string typ;
	string extrn_name;
	Variable v;
defloop:
	name = getname("name of variable");
	skipblank();
	if(look == ':'){
		match(':');
		skipblank();
		typ = gettype("type of variable \"" ~ name ~ "\"");
	}else{
		typ = bb_inttype;
	}
	
	if(look == '='){
		match('=');
		skipblank();
		match('"');
		extrn_name = getname("name of extern variable");
		match('"');

	}
	
	skipblank();
	v = Variable.add(true, name, typ);
	v.realname = extrn_name;

	if(look == ','){
		match(','); skipblank();
		goto defloop;
	}	
}

void var_definition(bool global){
	string name;
	string typ;
	Formula f;
	Variable v;
defloop:
	name = getname("name of variable");
	skipblank();
	if(look == ':'){
		match(':');
		skipblank();
		typ = gettype("type of variable \"" ~ name ~ "\"");
	}else{
		typ = bb_inttype;
	}
	if(look == ':'){
		getlook();
		if(look != '=') expected("':='");
	}
	if(look == '='){
		match('=');
		skipblank();
		f = Formula.parse();
	}else{
		f = null;
	}
	
	skipblank();
	v = Variable.add(global, name, typ);
	if(f)
		Function.current.addcode(new CAssignment(v, f));
	
	if(look == ','){
		match(','); skipblank();
		goto defloop;
	}
}
