
/*

	License note:

	Copyright (c)  2012  Christian Fiedler
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	Dieses Programm ist Freie Software: Sie koennen es unter den Bedingungen
	der GNU General Public License, wie von der Free Software Foundation,
	Version 3 der Lizenz oder (nach Ihrer Option) jeder spaeteren
	veroeffentlichten Version, weiterverbreiten und/oder modifizieren.

	Dieses Programm wird in der Hoffnung, dass es nuetzlich sein wird, aber
	OHNE JEDE GEWAEHRLEISTUNG, bereitgestellt; sogar ohne die implizite
	Gewaehrleistung der MARKTFAEHIGKEIT oder EIGNUNG FUER EINEN BESTIMMTEN
	ZWECK. Siehe die GNU General Public License fuer weitere Details.

	Sie sollten eine Kopie der GNU General Public License zusammen mit
	diesem Programm erhalten haben. Wenn nicht, siehe
	<http://www.gnu.org/licenses/>.
			
	This license note applies to all D source files in this and all lower
	directories and to all BlueBasic/BlitzMax code, because they are part
	of this software product.


	Main file. Home to int main(string[] args).
	
	Everything related to the lookahead, the current token, the 
	read-in process and general parsing procedures goes 
	here for the moment.
	Later, they shall be moved to corresponding modules.
	
	
	Common abbreviations:
		AST - Abstract Syntax Tree
*/

module main;

import std.stdio;
import std.ascii;
import std.conv;
import std.string;
import std.c.stdio;
import std.c.stdlib;
import std.file;

import backend_c;
import mvariable;
import mfunction;
import mtype;
import mconstant;
import mmisc;
import mcommand;
import mformula;
import mmodule;
import midentifier;
import mbackendman;
import cmdline;


uint line = 0;
char look;
string token;
string cs_token; // if case sensitivity is needed
bool blank_after_token = false;


int input_i = 0;

void getlook(){
	if(input_i == Module.current.input.length){
		look=0;
		return;
	}
	look = Module.current.input[input_i++];
	if(look == '\n') line++;
}

void skipwhite(){
	while(look == ' ' || look == '\t' || look == '\n' || look == '\r') getlook();
}

bool skipblank(){
	if(look != ' ' && look != '\t') return false;
	while(look == ' ' || look == '\t') getlook();
	return true;
}

// Skip blank chars and single line comments
void skipblankcm(){
	skipblank();
	if(look == '\'') read_till_eol();	
}

void skipline(){
	while(look != '\n' && look != 0) getlook();
	skipwhite();
}

void gettoken(){
	token.length = 0;
	cs_token.length = 0;
	if(isalpha(look)){
		while(isalnum(look)){
			token ~= toLower(look);
			cs_token ~= look;
			getlook();
		}
	}
	blank_after_token = skipblank();
}

void match(char x){
	if(look != x) expected(""~x, look);
	getlook();
}

void matchtoken(string x){
	if(token != toLower(x)) error(x ~ " expected (instead got \"" ~ token ~ "\")");
	skipblank();
}

string getname(string expects){
	string x;
	if(!isalpha(look)) expected(expects, look);
	while(isalnum(look)){
		x ~= look;
		getlook();
	}
	return x;
}

string gettype(string expects){
	string ret = getname(expects);
	if(bm_compatible_mode){
		switch(toLower(ret)){
		case "byte":
			ret = "ubyte";
			break;
		case "short":
			ret = "ushort";
			break;
		case "long":
			ret = "ulong";
			break;
		default:
		}
	}
	return ret;
}

string getnumber(string expects){
	string x;
	if(look == '-'){
		x="-";
		getlook();
	}
	if(!isdigit(look)) expected(expects, look);
	while(isdigit(look)){
		x ~= look;
		getlook();
	}
	if(look == '.'){
		x ~= '.';
		for(;;){
			getlook();
			if(!isdigit(look)) break;
			x ~= look;
		}
	}
	return x;	
}

/* skips a multiline comment */
void skipmlcomment(){
	for(;;){
		skipline();
		skipblank();
		gettoken();
		if(token == "endrem") return;
	}
}

string read_till_eol(){
	string r;
	while(look != '\n'){
		if(look != '\r') r ~= toLower(look); 
		getlook();
	}
	return r;
}

void assignment_expr(Variable vdest, string destop = null){
	Formula f;
	switch(look){
	case '=':
		getlook();
		skipblank();
		f=Formula.parse(destop, vdest);
		Function.current.addcode(new CAssignment(vdest, f));
		break;
	default:
		break;
	}
}



void identifier_function(string name){
	
	bool withbraces = false;
	Formula[] expressions;
	
	if(look == '(' && !blank_after_token) withbraces = true;
	
	if(withbraces) match('(');
	
	if(!((look == ')' && withbraces) || (isblank(look) && !withbraces))){

again:	expressions ~= Formula.parse();
	
		if(look == ','){
			getlook();
			skipblank();
			goto again;
		}

	}
	
	if(withbraces) match(')');
	
	Function.current.addcode(new CFunctionCall(name, expressions));
}

void identifier_var(string name){
	string destop;
	skipblank();
	switch(look){
	case ':':
		getlook();
		if(look != '='){
			if(isalpha(look)){
				destop = getname("operation");
			}else{
				destop = to!string(look);
			}
			look = '=';
		}
		assignment_expr(Variable.get(name), destop);
		break;
		
	case '=':
		assignment_expr(Variable.get(name));
		break;
		
	default:
		break;
	// ...
	}
}




static void debug_looktoken(){
	if(look < ' ')
		debugecho("[" ~ to!string(line) ~ "] look :\t" ~ to!string(cast(int) look));
	else
		debugecho("[" ~ to!string(line) ~ "] look :\t\'" ~ look ~ "\'");
	debugecho("[" ~ to!string(line) ~ "] token :\t" ~ token);
}

/*
	Does one "line" of the code at a time.
	Called by program()
*/
bool program_iteration(){
	
	debug_looktoken();
	
	switch(token){
	
	case "return":
		return_statement();
		break;
	
	case "import":
	case "module":
		skipline();	
		break;
	
	case "extern":
		do{
			skipline();
			gettoken();
		}while(token != "endextern");
		break;
		
	
	case "local":
	case "const":
	case "global":
		if(Function.current.curblock.length > 0)
			error("Declarations shall not appear in loops or conditions!");
		skipline();
		break;
	
	case "rem":
		skipmlcomment();
		break;
	
	case "function":
		enter_function();
		break;
		
	case "endfunction":
		Function.leave();
		break;
	
	case "if":
		if_statement();
		break;
		
	case "while":
		while_statement();
		break;
		
	case "repeat":
		repeat_statement();
		break;
		
	case "for":
		for_statement();
		break;
		
	case "goto":
		goto_statement();
		break;
		
	case "select":
		select_statement();
		break;
		
	case "exit":
		exit_statement();
		break;
		
	case "continue":
		continue_statement();
		break;
	
	case "case":
	case "default":	
	case "endselect":
	case "endif":
	case "finalize":
		return true;
	
	case "next":
	case "until":
	case "wend":
	case "elseif":
	case "else":
		if(Function.current.curblock.length == 0)
			error("Additional \'" ~ token ~ "\' closes block which is never opened");
		return true;
		
	default:
		if(token.length){
			if(Variable.estne(token)){
				identifier_var(token);
			}else if(Function.estne(token)){
				identifier_function(token);
			}
		}
		
	}
	
	return false;
}

/*
	Reads the actual executable code (preprogram only analyzes data and
	general structure) and encodes it to the AST.
*/
void program(){
	debugecho("program");
	while(look != 0){
	
		if(look == ';') getlook();
	
		skipwhite();
		
		if(look == 0) break;

		gettoken();
		
		if(program_iteration()) return;
		
		if(look == '\''){
			skipline();
		}else if(look == '#'){
			label_statement();
		}else{
			CLabel.active = null;
		}
	}
}

void import_statement(){
	string name;
	string ext;
	
	if(look == '"'){
		getlook();
		name = getname("source file name (import statement)");
		match('.');
		ext = getname("source file extension (import statement)");
		match('"');
		Module.current.import_source(name, ext);
		
	}else{
		name = getname("major module name");
		match('.');
		name ~= "." ~ getname("minor module name");
		Module.current.import_module(name);
	}
}

/*
	Prescans the source code for declarations and definitions. Thus 
	the compiler will know about all functions, variables, constants 
	etc. etc. when reading the code for the second time and there won't be 
	need for forward declarations.
*/
void preprogram(){
	while(look != 0){
	
		skipwhite();
		gettoken();
		debug_looktoken();

		
		switch(token){
		case "extern":
			enter_extern();
			debugecho("EXTERN");
			extern_preprogram();
			debugecho("ENDEXTERN");
			matchtoken("EndExtern");
			leave_extern();
			break;
		
			
		case "const":
			const_definition();
			break;
			
		case "global":
			var_definition(true);
			break;
			
		case "local":
			var_definition(false);
			break;
			
		case "function":
			function_definition();
			break;
			
		case "endfunction":
			if(Function.inmain()) 
				error("'endfunction' without 'function'");
			return;
		
		
		case "rem":
			skipmlcomment();
			break;
			
		default:
			
			if(look == '#')
				label_define();
			
			skipline();
			break;
		}
	}
}

void extern_preprogram(){
	while(look != 0){
	
		skipwhite();
		gettoken();
		debugecho("look : \'" ~ look ~ "\'");
		debugecho("token : " ~ token);
		
		switch(token){
		case "endextern":
			return;
			
		case "global":
			var_exdefinition();
			break;
			
		case "function":
			function_exdefinition();
			break;
		
		default:
			error("token \"" ~ cs_token ~ "\" cannot appear in an extern block");
			break;
		}
	}	
}


void search_imports(){
	input_i = 0;
	getlook();
	while(look){
		gettoken();
		if(token == "rem"){
			do{
				skipline();
				if(look == 0) error("Rem without EndRem");
				gettoken();
			}while(token != "endrem");
		}
		if(token == "import") import_statement();
		else skipline();
	}
}

/* Simple function that compiles what it finds in (string) input */
void compile(){

	void parse_module(Module m){
		if(m.modname)
			debugecho("- Parsing module " ~ m.modname ~ "." ~ m.name);
		else
			debugecho("- Parsing module \"" ~ m.name ~ "\"");
		
		m.enter();
	
		input_i = 0;
		line = 1;
	
		getlook();


		debugecho("parsing module \'" ~ m.name ~ "\'");

		preprogram();			
	}

	void parse_imports(Module mod){
		foreach(Module m; mod.imports){
			parse_module(m);
		}
	}
	
	check_abort2();
	
	search_imports();

	check_abort2();

	debugecho("Pass 1: Preparsing ...");
	parse_imports(Module.main);
	parse_module(Module.main);
	
	check_abort2();
	Module.current.compile_imports();
	
	Function.main = Module.main.funclist[0];
	foreach(Module m; Module.main.imports){
		Function.main.addcode(new CFunctionCall(m.funclist[0], null));
	}
	
	
	debugecho("Pass 2: Parsing ...");

	Module.current = Module.main;
	input_i = 0;
	line = 1;
	getlook();

	program();

	
	check_abort2();
	
	// call to inbound backend to produce the code
	line = 0;
	debugecho("Pass 3: Code Generation ...");
	emitcode(Module.main);
	
	debugecho("Giving orders to backend:");
	foreach(OutputFile of; OutputFile.list){
		debugecho("- compile \"" ~ of.name ~ "\"");
		Backend.compile(of);
	}
	
	if(!is_cmdoption(OPTION_NOLINK)){
		debugecho("Linking ...");
		if(build_app)
			Backend.link_app();
		else
			Backend.link_mod();
	}
}

int main(string[] args){
	// Initializing classes (by loading standard values)
	Type.init();

	backend_init();
	check_backend();

	do_cmdline(args);
	
	if(!Module.main) error("no sources found", true);
			
	compile();
	return 0;

}
