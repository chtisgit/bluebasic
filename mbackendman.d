/*

	Classes: Backend, ObjectFile, OutputFile
			
	A small module that handles the output files and preferences of the
	backend module.

*/

module mbackendman;

import std.stdio;
import std.string;
import std.process;
import std.array;
import std.file;

import mmisc;
import cmdline;
import mmodule;

class ObjectFile{
	string path;
	OutputFile from;
	
	this(OutputFile of){
		path = Backend.get_object_path(of.path);
		from = of;
		debugecho("NEW object file \"" ~ path ~ "\" from \"" ~ from.path ~ "\"");
	}
	
	this(string path){
		this.path = path;
		debugecho("NEW object file \"" ~ path ~ "\"");
	}

static:
	ObjectFile[] list;
	
	void add(ObjectFile of){
		list ~= of;
	}
}

class OutputFile{
	string name;
	string path;
	ObjectFile obj;
	File stream;


	this(string name, string path){
		this.name = name;
		this.path = path;

		debugecho("NEW output file \"" ~ name ~ "\"");
	}

static:
	OutputFile[] list;
	OutputFile current;
	OutputFile main;

	void add(OutputFile of){
		list ~= of;
	}
	
	OutputFile create(Module m){
		string npath;
		File ofile;
		npath = m.path ~ "." ~ Backend.file_ext;
		ofile = File(npath, "w");
		current = new OutputFile(npath[lastIndexOf(npath,"/")+1 .. $], npath);
		current.stream = ofile;
		return current;			
	}

	void close(OutputFile of = current){
		of.stream.close();
	}

}

abstract class Backend{
static:
	
	/* Backend has to specify if its target language knows ... */
	
	bool knows_continue;	// continue
	bool knows_exit;		// exit (or break)
	bool knows_goto;		// goto
	
	/*
		Backend has to specify the file extension of the output files
		for the target language
	*/	
	string file_ext;
	
	
	/*
		Backend has to implement a function that compiles the target
		language files using system() calls (for example)
	*/
	void function(OutputFile) compile;
	string function(string) get_object_path;
	void function() link_app;
	void function() link_mod;

	void emit(OutputFile of, string s){
		of.stream.write(s);
		if(is_cmdoption(OPTION_COUT)) writeln(s);
	}
	
	void emit(string s){
		emit(OutputFile.current, s);
	}

}

void check_backend(){
	try{
		assert(Backend.compile != null);
		assert(Backend.get_object_path != null);
		assert(Backend.link_app != null);
		assert(Backend.link_mod != null);
	}catch{
		error("Backend did not initialize correctly", true);
	}
}
