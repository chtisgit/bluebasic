/*
	

	Classes: Function
	
	Saves neccessary values for functions (and later methods) in an
	instance and provides functions to deal with them.

*/

module mfunction;

import std.conv;
import std.string;

import mvariable;
import mtype;
import mconstant;
import mmisc;
import main;
import mcommand;
import mmodule;
import midentifier;

class Function : Identifier{
public:
	Parameter[] param;
	Variable[] locals;
	TempVariable[] temps;
	/*	Each element in this array is also referenced by one element in the
		locals array.
		Variables in this array are used for special purposes by the compiler.
	*/
	
	Variable[] globals;
	Constant[] constlist;
	Command[] code;
	CBlockStatement[] curblock;
	CLabel[] labels;
	void delegate(Command c) code_redirect;	


	this(string name, string typ, string realname = null){
		super(name, typ, realname);
		debugecho("NEW function " ~ name ~ ":" ~ typ);
	}
	
	void add_global(Variable v){
		globals ~= v;
	}
	void add_local(Variable v){
		locals ~= v;
	}

	
	// adds code to deepest block in a function
	void addcode(Command c){
		if(code_redirect){
			code_redirect(c);
		}else{
			if(curblock.length == 0)
				code ~= c;
			else
				curblock[$-1].list ~= c;		
		}
}	
	
static:
	
	Function main;
	Function current;

	
	void add(Function f){
		Module.current.funclist ~= f;
	}
	
	void add_parameter(string name, string typ, string immvalue, bool optional, bool byref){
		debugecho("NEW parameter " ~ name ~ ":" ~ typ ~ " = \"" ~ immvalue ~ "\"");
		current.param ~= new Parameter(name, typ, optional, byref);
	}
	
	/*
		When the compiler (frontend!) needs a temporary variable in a function
		for any reason, it shall call this function and will receive one.
	*/
	TempVariable get_temp(string typ){
		TempVariable tnew;
		string name;
		foreach(TempVariable tmp; current.temps){
			if(!tmp.is_locked() && tmp.typ.name == typ) return tmp;
		}
		name = "temp" ~ to!string(current.temps.length);
		debugecho("create new temp '" ~ name ~ "' (" ~ current.name ~ ")");
		tnew = new TempVariable(name, typ);
		current.temps ~= tnew;
		current.locals ~= tnew;
		return tnew;
	}
	
	// TODO
	/*
	Function get(string name, Formula[] parameters){
		
	}
	*/
	
	// called by preprogram, only for syntax checking reasons
	void definelabel(string name){
		current.labels ~= new CLabel(name);
	}
	
	LoopBlock findloop(CLabel lab){
		LoopBlock lp;
		foreach(CBlockStatement c; current.curblock){
			if(cast(LoopBlock) c){
				lp = cast(LoopBlock) c;
				if(lp.prelabel.name == lab.name) return lp;
			}
		}
		return null;
	}
	
	CLabel findlabel(string name){
		name = toLower(name);
		foreach(CLabel lab; current.labels){
			if(toLower(lab.name) == name) return lab;
		}
		return null;
	}

	
	void enter_block(CBlockStatement block){
		CStatementIf ifblock;
		if(current.curblock.length > 0)
			ifblock = cast(CStatementIf) current.curblock[$-1];
		if(ifblock is null || ifblock.prev is null){
			current.addcode(block); // do not move this line!
			current.curblock ~= block;
		}else{
			current.curblock[$-1] = block;
		}
	}
	
	void enter(){
		if(current != main) error("nesting functions is not allowed");
		current = Module.current.funclist[$-1];
	}
	
	/* TODO Part I: should be extended. Won't work with overloading ... */
	void enter(string name){
		name = toLower(name);
		foreach(Function f; Module.current.funclist){
			if(toLower(f.name) == name){
				current = f;
				return;
			}
		}
	}
	
	bool estne(string name){
		return !check_name(name);
	}
	
	void leave(){
		current = main;
	}
	
	void leave_block(){
		LoopBlock l;
		if(cast(LoopBlock) current.curblock[$-1]) l = cast(LoopBlock) current.curblock[$-1];
		if(l) l.place_continue_label();
		current.curblock = current.curblock[0 .. $-1];
		if(l) l.place_exit_label();
	}
	
	
	bool check_name(string name){
		name = toLower(name);
		foreach(Function f; Module.current.funclist){
			if(f.name && toLower(f.name) == name) return false;
		}
		foreach(Module m; Module.current.imports){
			foreach(Function f; m.funclist){
				if(f.name && toLower(f.name) == name) return false;
			}
		}
		return true;
	}
	
	// TODO extend when overloading shall be implemented
	Function get(string name, uint len){
		name = toLower(name);
		foreach(Function f; Module.current.funclist){
			if(toLower(f.name) == name && f.param.length == len) return f;
		}
		foreach(Module m; Module.current.imports){
			foreach(Function f; m.funclist){
				if(toLower(f.name) == name && f.param.length == len) return f;	
			}
		}
		return null;
	}
	
	bool inmain(){
		return current == main;
	}
	
}


void function_exdefinition(){
	string name;
	string rtyp;
	string pname;
	string ptyp;
	string pimmval;
	
	name = getname("name of function");
	skipblank();
	
	if(look == ':'){
		match(':');
		skipblank();
		rtyp = gettype("return type of function \"" ~ name ~ "\"");
	}else{
		rtyp = bb_voidtype;
	}

	Function.add(new Function(name, rtyp, name));
	
	Function.enter();
	
	match('(');
	while(look != ')'){
		pname = null;
		if(look != ':') 
			pname = getname("name of parameter (function definition of \"" ~ name ~ "\")");
		match(':');
		ptyp = gettype("type of parameter \"" ~ pname ~ "\" (function definition of \"" ~ name ~"\")");

		skipblank();

		if(look == 'v'){
			if(getname(null) != "var"){ // getname will never fail, so null will do
				error("calling convention expected, but encountered something else in parameter list of function \"" ~ name ~ "\"");
			}
		}
		if(look == ','){
			Function.add_parameter(pname, ptyp, pimmval, false, false); 
			match(',');
			skipwhite();
			continue;
		}else
			break;
	}
	if(ptyp)
		Function.add_parameter(pname, ptyp, pimmval, false, false);

	match(')');
	skipblankcm();
	if(!isblank(look))
		error("additional characters on line (after declaration of \"" ~ name ~ "\")");
	

	Function.leave();	
}

void function_definition(){
	string name;
	string rtyp;
	string pname;
	string ptyp;
	string pimmval;
	bool popt;
	
	name = getname("name of function");
	skipblank();
	if(look == ':'){
		match(':');
		skipblank();
		rtyp = gettype("return type of function \"" ~ name ~ "\"");
	}else{
		rtyp = bb_voidtype;
	}

	Function.add(new Function(name, rtyp));
	
	Function.enter();
	
	match('(');
	if(isalpha(look)){
		for(;;){
			pname = getname("name of parameter (function definition of \"" ~ name ~ "\")");
			if(look == ':'){
				match(':');
				skipblank();
				ptyp = gettype("type of parameter \"" ~ pname ~ "\" (function definition of \"" ~ name ~"\")");
			}else{
				ptyp = bb_inttype;
			}
			skipblank();
			if(look == '='){
				match('=');
				skipblank();
				pimmval = getnumber("immediate value");
				popt = true;
			}else{
				popt = false;
			}
			if(look == 'v'){
				if(getname(null) != "var"){ // getname will never fail, so null will do
					error("calling convention expected, but encountered something else in parameter list of function \"" ~ name ~ "\"");
				}
				
			}
			if(look == ','){
				Function.add_parameter(pname, ptyp, pimmval, popt, false); 
				match(',');
				skipwhite();
				continue;
			}else
				break;
		}
		Function.add_parameter(pname, ptyp, pimmval, popt, false);
	}
	match(')');
	skipblankcm();
	if(!isblank(look))
		error("additional characters on line (after declaration of \"" ~ name ~ "\")");	
	


	preprogram();
	matchtoken("endfunction");

	Function.leave();
	
}

void enter_function(){
	/* TODO Part II: Again, won't work with overloading ... */
	string name = getname("name of function");
	skipline();
	Function.enter(name);
}



