/*

	This is a tiny backend that translates the AST into ANSI C code.

	The backend is quite different from the rest of the code.
	It should only provide a small set of functions that is accessible from outside.
	The other functions shall all be static, so that there are no name collisions with
	the frontend modules.
	This small set of functions currently only holds:
		void emitcode(Module) and
		void backend_init()
		
	backend_init is called BEFORE any parsing is done by the frontend. It should be used to
	set all the static variables in the class Backend, so the frontend can adapt its behavior
	to the backend.
	
	The module mbackendman contains the main interface for the backend. It currently
	provides functions to create, close and write to output files. As well as variables
	that save, how an output file shall be compiled (which compiler? which arguments? etc)
	
	THE BACKEND PROGRAMMER SHALL NOT USE HIS OWN OUTPUT FUNCTIONS! The purpose of the
	general backend interface is to standardize backends and to make it easy to write
	a backend by copying code from other backends.
	Feel free to use this backend for writing another!
	
	Although the backends shall only provide that small set of functions mentioned above, 
	any backend is able to use any function, class and variable from other modules, which
	are not declared static.
	On the one side it makes programming easier, because you have access to any data you need
	on the other side you must be careful to not use functions, that could cause the program
	to crash and the like.
	
	
*/


module backend_c;

import std.conv;
import std.system;
import std.string;
import std.process;

import cmdline;
import mvariable;
import mfunction;
import mtype;
import mmisc;
import mconstant;
import main;
import mcommand;
import mformula;
import mmodule;
import mbackendman;


static int indentlev=0;
static CBlockStatement[] curblock;

static string prologue = 
"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef void BB_Void;
typedef int BB_Int;
typedef unsigned long int BB_UInt;
typedef char BB_Byte;
typedef unsigned char BB_UByte;
typedef short BB_Short;
typedef unsigned short BB_UShort;
typedef long long BB_Long;
typedef unsigned long long BB_ULong;

#define EXPRESSION_ABS(X) (((X)<0)?(-(X)):(X))
#define EXPRESSION_SGN(X) (((X)<0)?-1:(((X)>0)?1:0))

";


static void emitln(string s, bool ind = true){
	emit(s ~ "\n", ind);
}
static void emit(string s, bool ind = false){
	if(ind) indent();
	Backend.emit(s);
}

static void indent(){
	for(int i = 0 ; i < indentlev ; i++){
		emit("\t");
	}
}

static void declare_func(Function f){
	emit(f.typ.realname ~ " " ~ f.realname ~ "(");
	if(f.param.length > 0){
		foreach(int i, Parameter p; f.param){
			emit(p.typ.realname ~ " " ~ p.realname);
			if(i < f.param.length-1) emit(",");
		}
	}else
		emit("void");
	emit(")");
}

static void define_var(Variable v, bool _static, bool _extern = false){
	if(_extern && _static)
		error("FATAL ERROR - WTF?");
	if(_extern) emit("extern ", true);
	if(_static) emit("static ", true);
	emitln(v.typ.realname ~ " " ~ v.realname ~ ";");
}

static void define_const(Constant c){
	if(c.typ.name == bb_voidtype)
		emitln("#define " ~ c.realname, false);
	else
		emitln("static const " ~ c.typ.realname ~ " " ~ c.realname ~
							" = " ~ c.value ~ ";");
}
static void undef_const(Constant c){
	if(c.typ.name == bb_voidtype)
		emitln("#undef " ~ c.realname, false);
}

static string[string] op_dict;


static void translate_powformula(Formula f){
	int i;
	for(i = 0; i < f.term.length-1; i++){
		emit("pow(");
		translate_term(f.term[i]);
		emit(", ");
	}
	translate_term(f.term[$-1]);
	for(i = 0; i < f.term.length-1; i++)
		emit(")");
}

static void translate_term(FTerm t){
	if(t is null) error("{MALFORMED AST} term is null");
	switch(t.typ){
	case FTerm.TermType.ImmediateValue:
		emit(t.value);
		break;
		
	case FTerm.TermType.Formula:
		emit("(");
		translate_formula(t.formula);
		emit(")");
		break;
	
	case FTerm.TermType.Variable:
		emit(t.var.realname);
		break;
		
	case FTerm.TermType.Constant:
		emit(t.con.realname);
		break;
		
	default:
		error("Backend error -- translate_formula");
	}
}

static void translate_formula(Formula f){
	int i;
	
	if(f.operation && f.operation[0] == "^"){
		translate_powformula(f);
		return;
	}
	
	for(i = 0 ; i < f.term.length ; i++){
		
		if(f.prefix){
			if(f.operation[i] in op_dict)
				emit(op_dict[f.operation[i]]);
			else
				emit(f.operation[i]);
			emit("(");
		}else if(i > 0){
			if(f.operation[i-1] in op_dict)
				emit(" " ~ op_dict[f.operation[i-1]] ~ " ");
			else
				emit(" " ~ f.operation[i-1] ~ " ");
		}
		
		translate_term(f.term[i]);
		
		if(f.prefix) emit(")");
		
	}
}

static void do_assignment(CAssignment a){
	emit(a.vdest.realname ~ " = ", true);
	translate_formula(a.formula);
	emitln(";", false);
}

static void emitprogramcode(Command[] commands){
	
	void do_whiles(CStatementWhile block){
		emit("while(", true);
		translate_formula(block.condition);
		emitln("){", false);
		
		indentlev++;
		emitprogramcode(block.list);
		indentlev--;
		
		emitln("}");	
	}
	
	void do_labels(CLabel lab){
		emitln(lab.realname ~ ": ;");
	}
	
	void do_jumps(CJump j){
		if(cast(CJExit) j){
			emitln("break;");
		}else if(cast(CJContinue) j){
			emitln("continue;");
		}else{
			emitln("goto " ~ j.label.realname ~ ";");
		}
	}
	
	void do_fors(CStatementFor block){
		emit("for(" ~ block.counter.realname ~ " = ", true);
		
		// Initial value

		translate_formula(block.fstart);

		emit(" ; ");
		
		// Condition (not quite well done yet)
		emit(block.counter.realname ~ " <= ");
		if(block.end)
			emit(block.end.realname);
		else
			translate_formula(block.fend);
		emit(" ; ");
		
		// Step value
		emit(block.counter.realname ~ " += ");
		if(block.step)
			emit(block.step.realname);
		else
			translate_formula(block.fstep);		


		emitln("){", false);
		
		indentlev++;
		emitprogramcode(block.list);
		indentlev--;
		
		emitln("}");
	}
	
	void do_repeats(CStatementRepeat block){
		emitln("do{");
		
		indentlev++;
		emitprogramcode(block.list);
		indentlev--;
		
		if(block.condition){
			emit("}while(!(", true);
			translate_formula(block.condition);
			emitln("));", false);
		}else{
			emitln("}while(1);");
		}
	
	}
	
	void do_return(CReturn ret){
		emit("return");
		if(ret.formula){
			emit(" ");
			translate_formula(ret.formula);
		}
		emitln(";");
	}
	
	void do_ifs(CStatementIf block){
		emit("if(", true);
		
	elseif_block:
		translate_formula(block.condition);
		emitln("){", false);

	else_block:
		indentlev++;
		emitprogramcode(block.list);
		indentlev--;
		
		emit("}", true);
		
		if(block.next){	// is there some statement belonging to this if?
			block = block.next;
			if(block.condition is null){
				emitln("else{", false);
				goto else_block;
			
			}else{
				emit("else if(");
				goto elseif_block;
			}
		}
		emitln("");
	}
	
	void do_blocks(CBlockStatement block){
		if(cast(CStatementIf) block){	// is BlockStatement = StatementIf ?
			do_ifs(cast(CStatementIf) block);
		}else if(cast(CStatementWhile) block){ // is BlockStatement = StatementWhile ?
			do_whiles(cast(CStatementWhile) block);
		}else if(cast(CStatementRepeat) block){
			do_repeats(cast(CStatementRepeat) block);
		}else if(cast(CStatementFor) block){
			do_fors(cast(CStatementFor) block);
		}		
	}
	
	void do_functioncall(CFunctionCall fc){
		emit(fc.func.realname ~ "(", true);
		foreach(Formula f; fc.parameters){
			translate_formula(f);
			if(f != fc.parameters[$-1]) emit(", ");
		}
		emitln(");", false);
	}

	CBlockStatement block;
	foreach(Command c; commands){
		// debugecho("Command Type: " ~ to!string(c.ctype));
		if(cast(CAssignment) c){	// is Command an assignment?
			do_assignment(cast(CAssignment) c);
			
		}else if(cast(CBlockStatement) c){ // is Command a block?
			do_blocks(cast(CBlockStatement) c);
			
		}else if(cast(CLabel) c){
			do_labels(cast(CLabel) c);
		
		}else if(cast(CJump) c){
			do_jumps(cast(CJump) c);
		
		}else if(cast(CReturn) c){
			do_return(cast(CReturn) c);
		
		}else if(cast(CFunctionCall) c){
			do_functioncall(cast(CFunctionCall) c);
		}
		
	}	
	
}

static void init_opdict(){
	op_dict["and"] = "&&"; op_dict["or"] = "||"; op_dict["mod"] = "%";
	op_dict["shr"] = ">>"; op_dict["shl"] = "<<"; op_dict["="] = "==";
	op_dict["<>"] = "!="; op_dict["~"] = "^"; op_dict["not"] = "!";
	op_dict["abs"] = "EXPRESSION_ABS"; op_dict["sgn"] = "EXPRESSION_SGN";

	op_dict.rehash();
}


void backend_init(){
	init_opdict();
	Backend.file_ext = "c";
	Backend.knows_continue = true;
	Backend.knows_exit = true;
	Backend.knows_goto = true;
	Backend.compile = &compile;
	Backend.get_object_path = &get_object_path;
	Backend.link_app = &link_app;
	Backend.link_mod = &link_mod;
}

static string get_object_path(string path){
	return path[0 .. lastIndexOf(path, ".")+1] ~ "o";
}

version(WINDOWS){
	const string c_compiler = "tcc";
}else{
	const string c_compiler = "gcc";
}
const string archiver = "ar";

static void compile(OutputFile file){
	string oname = get_object_path(file.path);
	debugecho(c_compiler ~ " -c " ~ file.path ~ " -o " ~ oname);
	system(c_compiler ~ " -c " ~ file.path ~ " -o " ~ oname);
}

static void link_app(){
version(WINDOWS){
	string oname = Module.main.dir ~ Module.main.name ~ ".exe";
}else{
	string oname = Module.main.dir ~ Module.main.name;
}
	string all;
	foreach(ObjectFile of; ObjectFile.list){
		if(of.from != OutputFile.main) all ~= of.path ~ " ";
	}
	debugecho("calling: " ~ c_compiler ~ " -o " ~ oname ~ " " ~ OutputFile.main.path ~ " " ~ all);
	system(c_compiler ~ " -o " ~ oname ~ " " ~ OutputFile.main.path ~ " " ~ all ~ "-lm");
}

static void link_mod(){
	string oname = Module.main.dir ~ Module.main.name ~ ".a";
	string all;
	foreach(ObjectFile of; ObjectFile.list){
		all ~= of.path ~ " ";
	}
	debugecho("calling: " ~ archiver ~ " r -o " ~ oname ~ " " ~ all);
	system(archiver ~ " r -o " ~ oname ~ " " ~ all);	
}

void emitcode(Module cmod){
	OutputFile of;
	if(cmod is null) return;

	of = OutputFile.create(cmod);
	OutputFile.add(of);
	if(cmod == Module.main) OutputFile.main = of;
	ObjectFile.add(new ObjectFile(of));

	emitln(prologue);
	
	foreach(Module m; cmod.imports){
		emitln("\n/* CONSTANT DEFINITIONS FROM " ~ toUpper(m.name) ~ " */");
		foreach(Constant c; m.funclist[0].constlist){
			define_const(c);
		}
		emitln("\n/* VARIABLE DEFINITIONS FROM " ~ toUpper(m.name) ~ " */");
		foreach(Variable v; cmod.funclist[0].globals){
			define_var(v, false, true);
		}
	}
	
	emitln("\n/* GLOBAL CONSTANTS */");
	foreach(Constant c; cmod.funclist[0].constlist){
		define_const(c);
	}	
	
	emitln("\n/* GLOBAL VARIABLES */");
	foreach(Variable v; cmod.funclist[0].globals){
		define_var(v, false);
	}

	

	foreach(Module m; cmod.imports){
		emitln("\n/* FUNCTION FORWARD DECLERATIONS FROM " ~ toUpper(m.name) ~ " */");
		foreach(Function f; m.funclist){
			declare_func(f);
			emitln(";");
		}
	}
	
	emitln("\n/* FUNCTION DECLARATIONS */");
	foreach(Function f; cmod.funclist){
		declare_func(f);
		emitln(";");
	}
	
	emitln("\n/* FUNCTION DEFINITIONS */");
	foreach(Function f; cmod.funclist){
		
		if(f.extrn) continue;
		
		// Function header
		declare_func(f);
		emitln("{");
		indentlev++;
		
		// Constants and globals
		if(Function.main != f){
			foreach(Constant c; f.constlist){
				define_const(c);
			}	
			foreach(Variable v; f.globals){
				define_var(v, true);
			}
		}
		
		// Locals
		foreach(Variable v; f.locals){
			define_var(v, false);
		}
		
		// The actual code
		emitprogramcode(f.code);

		
		// Undefine scoped constants
		if(Function.main != f){
			foreach(Constant c; f.constlist){
				undef_const(c);
			}
		}
		
		indentlev--;
		emitln("}\n\n");
	}
	
	OutputFile.close();
	
}
