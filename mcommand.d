/*

	Classes:
		- Command
			- CAssignment
			- CBlockStatement
				- CStatementIf
				- LoopBlock
					- CStatementFor
					- CStatementRepeat
					- CStatementWhile
			- CFunctionCall
			- CJump
				- CJContinue
				- CJExit
			- CLabel
				

	One of the most important and powerful modules in BlueBasic.
	All the executable commands are grouped into command blocks,
	which can be conditional (if, elseif, else, select, case, default) and
	which can be loops (repeat, while, for, foreach). 
	Exceptions of this rule: labels (which are not commands) and jumps 
		(including Exits and Continues)
	And not only these command blocks, but also their corresponding parser
	functions shall (in the end) go into this module.
	
	I describe some compiler specific details on the programming language here.
	
*/

module mcommand;

import std.conv;
import std.string;

import mfunction;
import mvariable;
import mformula;
import main;
import mmisc;
import mmodule;
import mmisc;
import midentifier;
import mtype;
import mbackendman;


abstract class Command{
}

/*
	Labels are not Commands. There is also no direct way to let
	the user "place" labels. They are used by the compiler for
	special purposes (e. g. Finalize-Statement, Exit/Continue)
	
	If the target language (into which the backend wants to transform
	the AST) cannot cope with labels and gotos, the statements which
	rely on them cannot be translated.
*/
class CLabel : Command{
	string name;
	string realname;
	
	this(){
		static int lcount = 0;
		name = "_" ~ to!string(lcount++);
		realname = name;
	}
	
	this(string name){
		this.realname = "_UDL_" ~ name;
		this.name = toLower(name);
	}

static:
	
	CLabel active = null;
}


/* creates the labels and puts them into the AST */
void label_statement(){
	string name;
	
	getlook();
	name = getname("label name");
	
	CLabel.active = new CLabel(name);
	Function.current.addcode(CLabel.active);
}

/* Called while preparsing - checks for name collisions*/
void label_define(){
	string name;
	
	getlook();
	name = getname("label name");
	
	if(Function.findlabel(name)){
		error("label \'" ~ name ~ "\' is defined (at least) twice");
		return;	
	}
	
	Function.definelabel(name);
	
}


class CJump : Command{
	CLabel label;
	
	this(){}
	this(CLabel lab){
		this.label = lab;
	}
}

/* Parses Gotos which are the only CJumps that branch to user defined labels */
void goto_statement(){
	string name;
	CLabel lab;

	if(!Backend.knows_goto) error("Your backend does not support Goto", true);

	name = getname("destination label (goto)");

	lab = Function.findlabel(name);
	if(!lab){
		error("label is never defined");
		return;
	}
	Function.current.addcode(new CJump(lab));
	
}

class CReturn : Command{
	Formula formula;
}

void return_statement(){
	CReturn r = new CReturn();
	skipblankcm();
	if(!isblank(look))
		r.formula = Formula.parse();
	Function.current.addcode(r);
}


/*

	CJExit and CJContinue are jump commands for the Exit and Continue statement
	They are derived from CJump to provide the possibility to expand BlueBasic, so that
	Languages that don't know the Exit/Break and/or Continue command (but instead goto/jmp) can
	also be used as target languages.

	This is not possible yet, because I want to focus on other things now.

*/

class CJExit : CJump{
}

void exit_statement(){
	CLabel lab;
	LoopBlock lp;
	string name;
	
	if(isalpha(look)){
		name = getname("label name (exit statement)");
		lab = Function.findlabel(name);
		if(!lab) error("label \"" ~ name ~ "\" does not exist");
	}
	if(lab){
		lp = Function.findloop(lab);
		lp.do_exit();
	}else{
		Function.current.addcode(new CJExit());
	}
}

class CJContinue : CJump{
}

void continue_statement(){
	CLabel lab;
	LoopBlock lp;
	string name;
	if(isalpha(look)){
		name = getname("label name (continue statement)");
		lab = Function.findlabel(name);
		if(!lab) error("label \"" ~ name ~ "\" does not exist");
	}
	if(lab){
		lp = Function.findloop(lab);
		lp.do_continue();
	}else{
		Function.current.addcode(new CJContinue());
	}
}

class CAssignment : Command{
	
	Variable vdest;
	Formula formula;
	
	
	this(Variable vdest, Formula formula){
		this.vdest = vdest;
		this.formula = formula;
	}

static:
	
	
	// could be useful. I wrote it when I thought I would need it, but I actually didn't.
	CAssignment assign(Variable vdest, Variable vsource){
		CAssignment a;
		Formula f;
		f = new Formula();
		f.term ~= new FTerm(vsource);
		a = new CAssignment(vdest, f);
		return a;
	}
}

class CFunctionCall : Command{
	Function func;
	Formula[] parameters;
	
	this(Function f, Formula[] parameters){		
		
		func = f;
		this.parameters = parameters;
		
	}	
	
	this(string name, Formula[] parameters){		
		
		func = Function.get(name, cast(uint) parameters.length);
		if(!func) error("Function \"" ~ name ~ "\" never declared");
		this.parameters = parameters;
		
	}
}

/*

	BLOCK STATEMENTS
	
	These are statements that ...
	
		1. contain other Commands
		2. can have a condition (most have one)
	
	Examples: If, While, For, ForEach, Repeat, Select ...
	
*/
class CBlockStatement : Command{

	Formula condition;
	Command[] list;
	
	this(){}
	this(Formula f){
		condition = f;
	}
	
}


/*
	IF

	There are two types of If statements.
	
	o Multi line If statement:
	
		If [condition] Then
			[commands ...]
		
		ElseIf [condition] Then
			[commands ...]
		
		Else
			[commands ...]
		
		Finalize
			[commands ...]
			
		EndIf
		
		- indefinite number of ElseIfs
		- Else block is optional
	
	o Single line If statement:
		
		If [condition] Then [command] Else [command]
		
		- Else block is still optional (has to appear at the same line as If)
	
	
	These two types are parsed in the frontend. The backend will not be able
	to differentiate between the two forms (unless it encounters an ElseIf-Block
	in an If statement, which means it was a multi line statement).
*/
class CStatementIf : CBlockStatement{
	CStatementIf next;
	// Else(If)s belonging to If-Statement.

	CStatementIf prev;
	// previous condition (may be if or elseif, not else!)
	
}

/* called by if_statement - parses single line if statements */
void slif_statement(CStatementIf ifblock){

	void program_oneline(){

again:	skipblank();

		gettoken();
	
		program_iteration();
		
		if(look == ';'){
			getlook();
			goto again;
		}
		
	}

	Function.enter_block(ifblock);
	
	program_oneline();
	
	skipblank();
	gettoken();

	if(token == "else"){
		ifblock.next = new CStatementIf();
		ifblock.prev = ifblock;
		ifblock = ifblock.next;
		Function.enter_block(ifblock);
		program_oneline();
		skipblank();
		gettoken();
	}
	
	if(token == "finalize" || token == "elseif")
		error("neither Finalize nor ElseIf are supported by single line If conditions");
	
	Function.leave_block();
	
}

/* called by if_statement - parses multiline if statements */
void mlif_statement(CStatementIf ifblock){

	CStatementIf startif = ifblock;
	Command[] templist; 
	bool elsewas = false;
	CLabel finalize;
		
	void redirect(Command c){
		templist ~= c;
	}
	
	Function.enter_block(ifblock);
	
	program();
	
	
	while(token == "elseif"){
		ifblock.next = new CStatementIf();
		ifblock.prev = ifblock;
		ifblock = ifblock.next;	
		
		ifblock.condition = Formula.parse();
		skipblank();
		gettoken();
		matchtoken("then");
		skipwhite();
		
		Function.enter_block(ifblock);
		program();
	}
	
	
	if(token == "else"){
		ifblock.next = new CStatementIf();
		ifblock.prev = ifblock;
		ifblock = ifblock.next;
		elsewas = true;
		Function.enter_block(ifblock);
		program();
	}

	
	if(token == "finalize"){
		if(Backend.knows_goto){	
			// if there was no else, create it and let it skip
			// the finalize block
			if(!elsewas){ 
				ifblock.next = new CStatementIf();
				ifblock.prev = ifblock;
				ifblock = ifblock.next;
				Function.enter_block(ifblock);
			}
			
			finalize = new CLabel();
			Function.current.addcode(new CJump(finalize));
			
			Function.leave_block();
			
			program();
			
			Function.current.addcode(finalize);
		
		}else{
				
			Function.leave_block();
			Function.current.code_redirect = &redirect;

			program();
			Function.current.code_redirect = null;
			
			ifblock = startif;
			while(ifblock && ifblock.condition){
				ifblock.list ~= templist;
				ifblock = ifblock.next;
			}

		}

	}else{
	
		Function.leave_block();
	}

	matchtoken("endif");
}

/* Looks which kind of if statement is used and then calls mlif_statement of slif_statement */
void if_statement(){
	CStatementIf ifblock = new CStatementIf();

	ifblock.condition = Formula.parse();
	skipblank();
	gettoken();
	matchtoken("then");
	skipblank();
	if(isblank(look)){
		skipwhite();
		mlif_statement(ifblock);
	}else{
		skipwhite();
		slif_statement(ifblock);
	}
}


/*
	SELECT
	
	In other programming languages known as switch statement.
	IMPORTANT NOTE: Select is translated into a CStatementIf!!!

	
	Select [condition]
	
	' equals If [condition] = [formula] Then ...

	Case [formula] 
		[commands ...]
	
	' equals If [condition]>=[formula1] And [condition]<[formula2] Then

	Case [formula1] To [formula2] 
		[commands ...]
	
	' if no "case" applied

	Default
		[commands ...]
		
	' if any of the "case"s applied

	Finalize	
		[commands ...]
	
	EndSelect
*/

void select_statement(){

	void do_case(CStatementIf selblock, Formula condition){
		Formula f, tmp1, tmp2;
	or_comma:
		tmp1 = Formula.parse();
		skipblank();
		gettoken();
		if(token == "to"){
			tmp2 = Formula.parse();
			if(f)
				f = Formula.or(f, Formula.inrange(condition, tmp1, tmp2));
			else
				f = Formula.inrange(condition, tmp1, tmp2);
		}else if(token == ""){
			if(f)
				f = Formula.or(f, Formula.equals(condition, tmp1));
			else
				f = Formula.equals(condition, tmp1);
		}else{
			error("inappropriate token in Case statement");
		}
		if(look == ','){
			getlook();
			goto or_comma;
		}
		selblock.condition = f;
	}
	
	CStatementIf startsel;
	CStatementIf selblock;
	Command[] templist;
	Formula condition;
	CLabel finalize;
	bool defwas;

	void redirect(Command c){
		templist ~= c;
	}

	skipblank();
	condition = Formula.parse();
	skipwhite();
	gettoken();
	if(token == "endselect") return;
	
	matchtoken("case");
	
	selblock = new CStatementIf();
	startsel = selblock;
	do_case(selblock, condition);
	
	Function.enter_block(selblock);	
	program();
	
	while(token == "case"){
		selblock.next = new CStatementIf();
		selblock.prev = selblock;
		selblock = selblock.next;
		do_case(selblock,condition);
		Function.enter_block(selblock);
		program();	
	}


	if(token == "default"){
		selblock.next = new CStatementIf();
		selblock.prev = selblock;
		selblock = selblock.next;
		
		selblock.condition = null;
		
		Function.enter_block(selblock);
		program();		
		
		defwas = true;
	}
	
	if(token == "finalize"){
		if(Backend.knows_goto){
			finalize = new CLabel();		
			if(!defwas){
				selblock.next = new CStatementIf();
				selblock.prev = selblock;
				selblock = selblock.next;
				selblock.condition = null;
				
				Function.enter_block(selblock);
			}
			Function.current.addcode(new CJump(finalize));
			Function.leave_block();
			
			program();
			
			Function.current.addcode(finalize);
		}else{
					
			Function.leave_block();
			Function.current.code_redirect = &redirect;

			program();
			Function.current.code_redirect = null;
			
			selblock = startsel;
			while(selblock && selblock.condition){
				selblock.list ~= templist;
				selblock = selblock.next;
			}		
		}
	}else{
		Function.leave_block();
	}
		
	matchtoken("endselect");
}


class LoopBlock : CBlockStatement{
	CLabel prelabel;
	CLabel continue_label, exit_label;
	
	this(){
		if(CLabel.active) prelabel = CLabel.active;
		continue_label = new CLabel();
		exit_label = new CLabel();
	}
	
	
	void place_continue_label(){
		if(!prelabel && Backend.knows_continue) return;
		Function.current.addcode(continue_label);
	}
	
	void place_exit_label(){
		if(!prelabel && Backend.knows_exit) return;
		Function.current.addcode(exit_label);
	}
	
	void do_continue(){
		Function.current.addcode(new CJump(continue_label));
	}
	
	void do_exit(){
		Function.current.addcode(new CJump(exit_label));
	}
}



/*
	WHILE LOOP
	
	The most simple pre-test loop in BlueBasic.
	
	While [condition]
		[commands ...]
	Wend
	
	Will not enter the loop if condition is false.
	
*/

class CStatementWhile : LoopBlock{

}

void while_statement(){
	CStatementWhile wloop = new CStatementWhile();
	
	wloop.condition = Formula.parse();
	skipwhite();
	
	Function.enter_block(wloop);
	
	program();
	
	matchtoken("wend");
	Function.leave_block();
}


/*
	REPEAT LOOPS

	o REPEAT - UNTIL
	
		The only loop in BlueBasic whose condition is verified
		at the end.
		
		Repeat
			[commands ...]
		Until [condition]
		
		Will leave the loop if condition is true.
	
	
	o REPEAT - FOREVER
	
		"endless" loop.
		
		Repeat
			[commands ...]
		Forever
		
		
		IMPORTANT NOTES REGARDING BACKENDS:
		
			- For the repeat-until block to work properly its condition has to be not-ed 
			by the backend when it is translated into a do-while construct!
			That is because Repeat-Until is left when the condition is true, whereas do-while
			is left when the condition is false.
			
			- If a Repeat loop is a Repeat-Until or Repeat-Forever loop can be
			determined by checking whether condition is NULL or not.
	
*/

class CStatementRepeat : LoopBlock{
		
}

/* Parses both kinds of Repeat loops */
void repeat_statement(){
	CStatementRepeat rloop = new CStatementRepeat();
	skipwhite();
	
	Function.enter_block(rloop);
	
	program();
	
	switch(token){
	case "forever":
		rloop.condition = null;
		break;
		
	case "until":
		skipblank();
		rloop.condition = Formula.parse();
		break;
		
	default:
		error("Repeat statement has to be closed with either 'Forever' or 'Until'");
	}
		
	Function.leave_block();
}

/*

	FOR LOOPS

	Every for loop is a counting loop, there are only minor differences:

	o For-To loop
	
		For [variable] := [formula1] To [formula2] Step [constant value] 
			[commands ...]
		Next
		
		[variable] counts from [formula1] to [formula2]
		
	o For-Until loop
		
		For [variable] := [formula] Until [formula] Step [constant value]
			[commands ...]
		Next
		
		[variable] counts from [formula1] to ([formula2]-1)
		
	o Notes regarding both:
	
		This variant of loop is restricted to integer types (of different
		sizes though: 8/16/32 bit shall work) and floating-point numbers.
		
		When the loop is entered for the first time, the results of the
		two formulas are saved, unless they are constant anyway.
		So it is not possible to change the results inside of the loop.
		
		
	IMPORTANT: The For-EachIn loop, which looks like a for loop, is not
	parsed into a CStatementFor but into a CStatementForEach (which does not exist yet).
	
	This is because the For-loop is a counting loop and should remain one.
	BlueBasic will allow the For-EachIn loop because it's the way BlitzMax
	did it and I won't force anyone to use the new ForEach loop, but it
	will be marked as deprecated.
	You can read below what this loop is all about.
		
*/

class CStatementFor : LoopBlock{
	Variable counter;
	TempVariable end, step;
	Formula fstart, fend, fstep;
}

/* Parses the For loop (For-Until not implemented yet) */
void for_statement(){
	CStatementFor floop = new CStatementFor();
	Formula f;
	string name;

	skipblank();	// get loop counter
	name = getname("loop counter");
	floop.counter = Variable.get(name);
	if(floop.counter is null) error("loop counter variable \"" ~ name ~ "\" never defined");
	
	skipblank();	// skip the mandatory = or :=
	if(look == ':') getlook();
	match('=');
	
	skipblank();
	
	floop.fstart = Formula.parse();
	
	gettoken();
	matchtoken("to");
	
	f = Formula.parse();
	
	if(f.is_constant()){
		floop.fend = f;
	}else{
		floop.end = Function.get_temp(bb_inttype);
		floop.end.lock();
		Function.current.addcode(new CAssignment(floop.end, f));	
	}
	
	skipblankcm();
	if(!isblank(look)){ // no newline --> Step is a MUST 
		gettoken();
		matchtoken("step");
		f = Formula.parse();
		
		if(f.is_constant()){
			floop.fstep = f;
		}else{
			warning("Step value is not constant, it will be saved before the first loop iteration");
			floop.step = Function.get_temp(bb_inttype);
			floop.step.lock();
			Function.current.addcode(new CAssignment(floop.step, f));
		}
	}else{
		floop.fstep = new Formula();
		floop.fstep.term ~= new FTerm("1", true); // step defaults to 1
	}
		
	Function.enter_block(floop);
	
	program();
	
	matchtoken("next");
	Function.leave_block();
	debugecho("open blocks: " ~ to!string(Function.current.curblock.length));
	if(floop.end) floop.end.unlock();
	if(floop.step) floop.step.unlock();
	
}


/*

	FOREACH loop
	
	Iterates over a bunch of Elements. It can be used to walk through
	strings or arrays (perhaps later also other entities).
	
	ForEach x In [1, 1, 2, 3, 5, 8, 13]
		Print x
	Next
	
	ForEach c In str
		Print c ' print ASCII/UTF8 codes of string
	Next
	
	These operations can be done with the deprecated For-EachIn loop too,
	that differs from the former only in the header:
	
	For x = EachIn [1, 1, 2, 3, 5, 8, 13]
	
	For c = EachIn str
	
	
	Why is this kind of loop abandoned?
	The new form is clearer and is an adaption of this kind of loop to
	the new (yet planned) "In" operator.

*/
