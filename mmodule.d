/*
	
	Classes: Module
	
	Copes with all the modules imported to the main source
	file. A module can be another BlueBasic source file, a directory
	in the mod folder (which does not exist yet) or a yet compiled
	object file (*.obj or *.o).
	
	
*/

module mmodule;

import std.array;
import std.file;
import std.conv;
import std.string;
import std.process;

import main;
import cmdline;
import mfunction;
import mmisc;
import mconstant;
import mvariable;
import mtype;
import mbackendman;

/*
	Based on this "flag" other parts of the parser can determine, whether
	the compiler is compiling a '.bmx' or a '.blub' file, so treatment of
	these two types of files can differ.
*/
bool bm_compatible_mode = false;
bool build_app = true;

class Module{
	
	string modname, name;
	string obj_name;
	string prefix;
	string dir;
	string path;
	bool isfile;
	Function[] funclist;
	string input;
	Module[] imports;
	
	// Only BlueBasic source files are supported yet!!
	this(string name){
		if(endsWith(name, ".bmx") || endsWith(name, ".blub")){
			this.path = name;
			if(indexOf(name, "/") >=0)
				dir = name[0 .. lastIndexOf(name, "/")+1];
			else
				dir = "";
				
			if(indexOf(name, "/")>=0) name = name[lastIndexOf(name,"/")+1 .. $];
			this.name = name[0 .. lastIndexOf(name, '.')];
			this.prefix = "__src" ~ this.name;
			this.isfile = true;
			this.obj_name = Backend.get_object_path(this.path);
			
		}else if(isDir(module_to_path(name))){
			
			this.name = name[indexOf(name, ".")+1 .. $];
			this.modname = name[0 .. indexOf(name, ".")];
			this.prefix = "_mod" ~ this.modname ~ "_" ~ this.name;
			
			this.dir = module_to_path(name);
	
			this.path = this.dir ~ this.name;
			
			if(exists(this.path ~ ".bmx"))
				this.path ~= ".bmx";
			else if(exists(this.path ~ ".blub"))
				this.path ~= ".blub";
			else
				error("Module has no main file", true);
				
			this.isfile = false;
			
		}
		
		input = to!string(std.file.read(path));
			
		debugecho("Module: " ~ this.name);
		debugecho("Path: " ~ this.path);
	}
	
	void compile(){
		if(isfile && (timeLastModified(path) > timeLastModified(obj_name) ||
		 is_cmdoption(OPTION_FORCERECOMPILE)))
		 	system(compiler_name ~ " " ~ OPTION_NOLINK ~ " " ~ path);
	}
	
	void compile_imports(){
		foreach(Module m; imports){
			m.compile();
		}
	}
	
	void import_source(string name, string ext){
		string sname;
		OutputFile of;
		// TODO
		if(ext == "blub" || ext == "bmx"){

		}else if(ext == Backend.file_ext){
			sname = name ~ "." ~ ext;
			of = new OutputFile(sname, dir ~ sname);
			OutputFile.add(of);
			ObjectFile.add(new ObjectFile(of));
		}
	}
	
	void import_module(string name){
		Module newmod;
		debugecho(current.name ~ " IMPORTS " ~ name);
		newmod = new Module(name);
		imports ~= newmod;
		ObjectFile.add(new ObjectFile(newmod.get_interface()));
	}
	
	bool doesimport(Module mod){
		foreach(Module m; imports){
			if(m == mod) return true;
		}
		return false;
	}

	
	void enter(){
		if(endsWith(path,".bmx"))
			bm_compatible_mode = true;
		else
			bm_compatible_mode = false;
		current = this;
		
		if(funclist.length == 0){
			if(current == main && build_app)
				Function.main = new Function(null, "void", "bb_main");
			else
				Function.main = new Function("main", "void");
			Function.current = Function.main;
			current.funclist ~= Function.main;
		}
	}
	void leave(){
		if(current == main) error("{FRONTEND} current == main"); // should not happen
		current = main;
	}
	
	string get_interface(){
		string modshort = modname ~ "." ~ name;
		string path = "mod/" ~ modname ~ ".mod/" ~ name ~ ".mod/";
		
		if(!exists(path)) error("module " ~ modshort ~ " does not exist");
		
		path ~= name ~ ".a";
		
		if(!exists(path)) error("interface for module " ~ modshort ~ " does not exist");
		
		return path;	
	}
	
static:
	Module main;
	Module current;
	
	void setmain(string name){
		main = new Module(name);
		current = main;
	}
	
	void create_archive(){
		string aname = main.dir[0 .. $-1];
		aname ~= "/" ~ aname[lastIndexOf(aname, "/")+1 .. $];
		aname = aname[0 .. lastIndexOf(aname, ".")] ~ ".a";
		if(exists(aname)) remove(aname);
		
		//TODO
	}
	
	string module_to_path(string mod){
		return "mod/" ~ replace(mod, ".", ".mod/") ~ ".mod/";
	}
	
	
	void link_all(){
		debugecho("Linking ...");
		//TODO
	}
	
	void compilemodule(string name){
		setmain(name);
		build_app = false;
	}

	bool is_name_used(string name){
		if(!Function.check_name(name)) return true;
		if(!Variable.check_name(name)) return true;
		if(!Constant.check_name(name)) return true;
		return false;
	}

}


