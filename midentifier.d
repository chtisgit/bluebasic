/*
	
	Class: Identifier
	
	Super class for Constant, Variable and Function.
	
*/

module midentifier;

import mtype;
import mmisc;
import mmodule;


abstract class Identifier{
	string name, realname;
	Type typ;
	bool extrn;
	
	Module mod;	// module of definition
	int line;	// line of definition
	
	this(string name, string typ, string realname = null){
		this.name = name;
		this.mod = Module.current;
		if(realname)
			this.realname = realname;
		else
			this.realname = mod.prefix ~ "_" ~ name;
		this.typ = Type.gettype(typ);
		if(this.typ is null) error("type \"" ~ typ ~ "\" (used as type of identifier \"" ~ name ~ "\") does not exist");
		this.extrn = in_extern_block;
		this.line = line;
	}
	
}

// Extern Blocks (Management)


static bool in_extern_block = false;

void enter_extern(){
	if(in_extern_block) error("Extern blocks cannot be nested");
	in_extern_block = true;
}

void leave_extern(){
	if(!in_extern_block) error("EndExtern without Extern");
	in_extern_block = false;
}

bool is_extern(){
	return in_extern_block;
}


