
# Small Makefile

CC = dmd
LD = ld


SRC = $(shell ls *.d)
OBJ = $(addsuffix .obj, $(basename $(SRC)))

debug: $(SRC)
	$(CC) $(SRC) -cov -debug -unittest -w -ofbbc

release: $(SRC)
	$(CC) $(SRC) -release -w -ofbbc

.PHONY: clean
clean:
	rm -rf $(OBJ)
