
module mmisc;

import std.stdio;
import std.ascii;
import std.conv;
import std.c.stdlib;

import main;
import mmodule;
import cmdline;

static int errors = 0;


bool isdigit(char look){
	return isDigit(look);
}

bool isalpha(char look){
	return look=='_' || isAlpha(look);
}

bool isalnum(char look){
	return look=='_' || isAlphaNum(look);
}

bool isblank(char look){
	return look == '\n' || look == '\r' || look == ' ' || look == '\t';
}

void abort_compilation(){
	writeln("Compilation aborted.");
	exit(1);
}

/* would not build with name check_abort */
void check_abort2(){
	if(errors > 0) abort_compilation();
}

void expected(string exp, char got = 0){
	string err = exp ~ " expected";
	if(got) err ~= ", instead got '" ~ got ~ "' [" 
		~ to!string(cast(int) got) ~ "]";

	error(err);
}

void warning(string s){
	if(is_cmdoption(OPTION_WARNINGASERR))
		error(s);
	else
		writeln("\n[" ~ to!string(line) ~ "] warning: " ~ s);
}

void error(string s, bool fatal = false){
	if(look=='\n') line--;
	write("\n");
	if(line) write("[" ~ to!string(line) ~ "] ");
	writeln("error: " ~ s);
	if(fatal) abort_compilation();
	errors++;
	if(Module.current.input.length > 0) skipline();
}

void debugecho(string s){
	if(is_cmdoption(OPTION_DEBUGENABLED))
		writeln(s);
}

string get_realname(string name, Module m){
	return "_" ~ m.name ~ "_" ~ name;	
}
