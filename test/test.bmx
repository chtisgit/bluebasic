Rem
	
	A test file.
	Test comment.
	' Test comment in test comment.
	
EndRem

Import blub.core

Const not_the_answer = 43 ' Test constant
Global a = 1,b,c ' Test variables ...
Local d,e
Global CHeck_CASEinsenSiTiViTy

Function f() ' test function
	Const ij = 42
	Local i
	Global j ' test global in test function
EndFunction

Function zee:Int(PARAMETER1:Int, parameter2, parameter3 = 5)
	Const style ' test void constant in test int function
	Const answer = 42
	Local x,y,max
	Global z
	y := 1
	z = not_the_answer/2
	If y = 1 And z = 21 Then
		x := 50 + (answer - y) * max + 5 Mod 50
	ElseIf answer = 42 Or y = 2 Then
		x :+ not_the_answer * z
	ElseIf answer <> 42 Then ' oh no!
		#labeled_loop
		While x < answer
			x:+1
		Wend
	Else
		x := answer
		max = CHECK_CASEINSENSITIVITY
	Finalize
		a = x
		x = check_caseinsensitivity
	EndIf
EndFunction

c=3

' Repeat Until
Repeat
	a:+c
Until a>200

#wiederhole
e=20
' For loop
For d = e To 50 Step a
	b:+1
	e:*2 ' This will not affect the loop!
Next

If b<50 Then c=42;Goto wiederhole Else b=0 ' single line If


'Select

Select d+e+42
Case 42
	a = 42
Case 0 To a
	a = d+e+42
Default
	a:-1
Finalize
	d = 42-e
EndSelect

