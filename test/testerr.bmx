Rem
	
	A test file.
	Test comment.
	' Test comment in test comment.
	
EndRem

Const zee = 43 ' Test constant
Global a,b,c ' Test variables ...
Local d,e,c

Function f(x:Object) ' test function
	Const ij = 42
	Local i
	Global j ' test global in test function
EndFunction

Function zee:Int(PARAMETER1:Int, parameter2, parameter3 = 5,)
	Const style ' test void constant in test int function
	Local x,y
	Global z
EndFunction