/*

	A small module which provides functions and variables
	to deal with command line options.

*/

module cmdline;

import std.stdio;
import std.string;
import std.c.stdlib;
import std.file;
import std.array;

import main;
import mmodule;
import mmisc;


const string OPTION_DEBUGENABLED	= "-d";
const string OPTION_WARNINGASERR	= "-w";
const string OPTION_NOCHECK			= "--no-check";
const string OPTION_FORCERECOMPILE	= "--frc";	
const string OPTION_NOLINK			= "-c";
const string OPTION_CHILD			= "--child";
const string OPTION_COUT			= "-y";

string compiler_name;

static string[] options;

static void printhelptext(){
	writeln(
"
bbc.exe [options] file.bmx|file.blub

--help, -h			shows this message
--no-check			does not check the AST for errors
				(makes compilation faster but more unstable)
--frc				force recompilation of all modules of a project
-c					compile only; do not link
-w					treat warnings as errors
-d					display compiler debug messages (includes -v)
...
"	
	);
	exit(0);
}

bool is_cmdoption(string opt){
	foreach(string s; options){
		if(opt == s) return true;
	}
	return false;
}


void do_cmdline(string[] args){
	string name;
	compiler_name = args[0];
	foreach(string arg; args){
		if(!Module.main && indexOf(arg, ".") > 0){
			name = "mod/" ~ replace(arg, ".", ".mod/") ~ ".mod";
		 	if((arg.endsWith(".blub") || arg.endsWith(".bmx")) && exists(arg)){
				if(arg.indexOf('\\') >= 0) arg = replace(arg, "\\", "/");
				Module.setmain(arg);
			}else if(exists(name)){
				Module.compilemodule(arg);
			}
		}else if(arg.startsWith("-")){
			switch(arg){
			case "--help":
			case "-h":
				printhelptext();
				break;
			default:
				options ~= arg;
				break;
			}
		}
	}
}
