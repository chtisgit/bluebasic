/*

	Classes: Constant
	
	Saves neccessary values for constants in an
	instance and provides functions to deal with them.
	
*/

module mconstant;

import std.string;

import mmisc;
import mfunction;
import mtype;
import main;
import mmodule;
import midentifier;

class Constant : Identifier{
public:
	string value;

	
	this(string name, string typ, string value){
		super(name, typ);
		if(this.typ is null) error("type \"" ~ typ ~ "\" (of const \"" ~ name ~ "\") does not exist");
		this.value = value;
	}


static:
	
	void add(string name, string typ, string value){
		debugecho("NEW constant " ~ name ~ ":" ~ typ ~ " = \"" ~ value ~ "\"");
		if(Module.is_name_used(name)) error("identifier \"" ~ name ~ "\" is defined (at least) twice");
		Function.current.constlist ~= new Constant(name, typ, value);
	}
		
	bool check_name(string name){
		name = toLower(name);
		// if(Function.current is null) error("Function current = null",true); // perhaps unneeded 
		foreach(Constant c; Function.current.constlist){
			if(toLower(c.name) == name) return false;
		}
		return true;
	}
	
	Constant get(string name){
		name = toLower(name);
		foreach(Constant c; Function.current.constlist){
			if(toLower(c.name) == name) return c;
		}
		foreach(Constant c; Function.main.constlist){
			if(toLower(c.name) == name) return c;
		}
		return null;
	}
}

void const_definition(){
	string name;
	string typ;
	string val;
	bool expltyp;
defloop:
	val.length=0;
	name = getname("name of constant");
	skipblank();
	if(look == ':'){
		match(':');
		skipblank();
		typ = gettype("type of constant \"" ~ name ~ "\"");
		expltyp = true;
	}else{
		typ = bb_voidtype;
		expltyp = false;
	}
	skipblank();
	if(look == '='){
		if(typ == bb_voidtype){
			typ = bb_inttype;
			if(expltyp) error("Void constant cannot have value");
		}
		match('=');
		skipblank();
		if(typ == bb_inttype){
			val = getnumber("integer");
		}
	}
	Constant.add(name, typ, val);
	skipblank();
	if(look == ','){
		match(','); skipblank();
		goto defloop;
	}
}

