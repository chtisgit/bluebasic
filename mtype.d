/*

	Classes: Type
	
	Saves neccessary values for primitive types in an
	instance and provides functions to deal with them.
	Will be used later for user defined types too.
	
*/

module mtype;
import std.string;
import mmisc;
import mmodule;
import main;

const string bb_voidtype	= "Void";
const string bb_inttype		= "Int", bb_uinttype		= "UInt";
const string bb_floattype	= "Float";
const string bb_doubletype	= "Double";
const string bb_int8type	= "Byte", bb_uint8type		= "UByte";
const string bb_int16type	= "Short", bb_uint16type	= "UShort";
const string bb_int64type	= "Long", bb_uint64type		= "ULong";


class Type{
public:
	string name;
	string realname;
	
	this(string name, string realname = null){
		this.name = name;
		if(realname is null)
			this.realname = "BB_" ~ name;
		else
			this.realname = realname;
	}
	
static:

	Type[] list;
	
	Type gettype(string name){
		name = toLower(name);
		foreach(Type t; list){
			if(toLower(t.name) == name) return t;
		}
		return null;
	}
	
	void init(){
		list =	[new Type(bb_voidtype), new Type(bb_inttype), 
				 new Type(bb_uinttype), new Type(bb_floattype),
				 new Type(bb_doubletype), new Type(bb_int8type),
				 new Type(bb_int16type), new Type(bb_int64type),
				 new Type(bb_uint8type), new Type(bb_uint16type),
				 new Type(bb_uint64type)];
	}
	
}
