/*
	
	Classes: Formula, FTerm
	
	Saving maths.
	
	I am not quite sure if the current setup will work with subsequent
	changes and expansion.
	
*/

module mformula;

import std.conv;
import std.string;
import std.ascii;

import main;
import mvariable;
import mfunction;
import mtype;
import mmisc;
import mconstant;



class FTerm{

	enum TermType{
		ImmediateValue, Formula, Variable, Constant
	}
	
	TermType typ;
	
	union{
		string value;
		Formula formula;
		Variable var;
		Constant con;
	}
	
	this(Variable v){
		this.var = v;
		this.typ = TermType.Variable;
	}
	
	this(string value, bool isval){
		if(isval){
			this.value = value;
			typ = TermType.ImmediateValue;
		}else{
			this.var = Variable.get(value);
			if(this.var is null){
				this.con = Constant.get(value);
				typ = TermType.Constant;
				if(this.con is null)
					error("unknown identifier " ~ value);
			}else{
				typ = TermType.Variable;
			}
		}
	}
	
	bool is_constant(){
		if(typ == TermType.ImmediateValue || typ == TermType.Constant)
			return true;
		else if(typ == TermType.Formula)
			return formula.is_constant();
		else
			return false;
	}
	
	this(Formula f){
		this.formula = f;
		typ = TermType.Formula;
	}
	
}

// not sure, if this is correct... 
static void shiftback(string s){
	if(look == '\n') line--;
	input_i -= s.length+1;
	getlook();	
}



class Formula{
	FTerm[] term;
	string[] operation;
	bool prefix = false;

	bool is_constant(){
		foreach(FTerm t; term){
			if(!t.is_constant()) return false;
		}
		return true;
	}

private:

	
	
public:	
static:

	Formula equals(Formula f, Formula f2){
		Formula ret = new Formula();
		ret.term = [new FTerm(f), new FTerm(f2)];
		ret.operation = ["="];
		return ret;
	}
	
	Formula inrange(Formula f, Formula f1, Formula f2){
		Formula fp1 = new Formula();
		Formula fp2 = new Formula();
		fp1.term = [new FTerm(f), new FTerm(f1)];
		fp1.operation = [">="];
		fp2.term = [new FTerm(f), new FTerm(f2)];
		fp2.operation = ["<="];
		return and(fp1, fp2);
	}

	Formula or(Formula f, Formula f2){
		Formula ret = new Formula();
		ret.term = [new FTerm(f), new FTerm(f2)];
		ret.operation = ["or"];
		return ret;
	}

	Formula and(Formula f, Formula f2){
		Formula ret = new Formula();
		ret.term = [new FTerm(f), new FTerm(f2)];
		ret.operation = ["and"];
		return ret;
	}


	FTerm parseV(){
		FTerm ret;
		string tmp;
		if(isdigit(look)){
			ret = new FTerm(getnumber(null), true);
		}else if(isalpha(look)){
			tmp = getname(null).toLower();
			if(look == '('){
				// Function call
			}else{
				// Variable or Constant
				ret = new FTerm(tmp, false);
			}
		}else if(look == '('){
			getlook();
			ret = new FTerm(parse0());
			skipblank();
			if(look != ')') expected(")");
			getlook();
			skipblank();
		}
		return ret;
	}
	
	FTerm parseH(){
		Formula f = new Formula();
		string op;
		
		f.prefix = true;
		
		skipblank();
		
		if(look == '~'){
			f.operation ~= to!string(look);
		
		}else if(look == '-'){
			f.operation ~= to!string(look);	
		
		}else if(isalpha(look)){
			
			op = getname(null).toLower();
			if(op != "not" && op != "sgn" && op != "abs"){
				shiftback(op);
				return parseV();
			}
			
			f.operation ~= op;
			
		}else{
			return parseV();
		}

		getlook();
		skipblank();
		f.term ~= parseV();		
		return new FTerm(f);
	}
	
	FTerm parsePow(){
		Formula f = new Formula();
		string op;
		skipblank();
		f.term ~= parseH();
		skipblank();
		if(look != '^') return f.term[0];
		while(look == '^'){
			getlook();
			skipblank();
			f.term ~= parseH();
			f.operation ~= "^";
			skipblank();
		}
		return new FTerm(f);		
	}
	
	FTerm parse4(){
		Formula f = new Formula();
		string op;
		skipblank();
		f.term ~= parsePow();
		skipblank();
		if(look != '*' && look != '/' && toLower(look) != 'm' && toLower(look) != 's')
				return f.term[0];
		while(look == '*' || look == '/' || toLower(look) == 'm' || toLower(look) == 's'){
			op = to!string(look);
			if(toLower(look) == 'm' || toLower(look) == 's'){
				op = getname(null).toLower();
				if(op != "mod" && op != "sar" && op != "shr" && op != "shl"){
						shiftback(op);
						op.length = 0;
				}
			}
			if(op.length == 0) break;
			getlook();
			skipblank();
			f.term ~= parsePow();
			f.operation ~= op;
			skipblank();
		}
		return new FTerm(f);
	}
	
	FTerm parse3(){
		Formula f = new Formula();
		char op;
		skipblank();
		f.term ~= parse4();
		skipblank();
		if(look != '+' && look != '-') return f.term[0];
		while(look == '+' || look == '-'){
			op = look;
			getlook();
			skipblank();
			f.term ~= parse4();
			f.operation ~= to!string(op);
			skipblank();
		}
		return new FTerm(f);
	}
	
	FTerm parse2(){
		Formula f = new Formula();
		char op;
		skipblank();
		f.term ~= parse3();
		skipblank();
		if(look != '&' && look != '|' && look != '~') return f.term[0];
		while(look == '&' || look == '|' || look == '~'){
			op = look;
			getlook();
			skipblank();
			f.term ~= parse3();
			f.operation ~= to!string(op);
			skipblank();
		}
		return new FTerm(f);		
	}
	
	FTerm parse1(){
		Formula f = new Formula();
		string op;
		skipblank();
		f.term ~= parse2();
		skipblank();
		if(look != '>' && look != '<' && look != '=') return f.term[0];
		while(look == '>' || look == '<' || look == '='){
			op = to!string(look);
			getlook();
			if(look == '<' || look == '>' || look == '='){
				op ~= look;
				if(op == "=>") op = ">=";
				if(op == "=<") op = "<=";
				if(op != ">=" && op != "<>" && op != "<="){
						error("operator " ~ op ~ "not supported");
				}
				getlook();
			}
			skipblank();
			f.term ~= parse2();
			f.operation ~= op;
			skipblank();
		}
		return new FTerm(f);
	}
	
	Formula parse0(){
		Formula f = new Formula();
		string op;
		skipblank();
		f.term ~= parse1();
		skipblank();
		while(toLower(look) == 'a' || toLower(look) == 'o'){
			op = getname(null).toLower();
			if(op == "and" || op == "or"){
				getlook();
				skipblank();
				f.term ~= parse1();
				f.operation ~= op;
				skipblank();
			}else{
				shiftback(op);
				break;
			}
		}
		return f;		
	}
	
	
/*
	Explaining destops:
	
	destops are a way of making an expression like:
	
	x = x + 1
	
	shorter. In BlueBasic this works with ":" and the operator you want to use:
	
	x :+ 1
	
	These operations cannot appear in conditions. That is why the parameters
	destop and dest (in parse) are optional. 
	
	IMPORTANT NOTE!
	The frontend parses these destops into ordinary expressions, so the
	backend will not be able to find out, whether destops have been used.
	
	[destop is an artificial word which is made up by destination and operation,
	because the destination identifier is reused in a "hidden" operation without
	being mentioned twice]
	
*/
	
	Formula parse(string destop = null, Variable dest = null){
		Formula f = parse0();
		if(destop){
			f.operation = destop ~ f.operation;
			f.term = new FTerm(dest) ~ f.term;
		}
		if(look == ')'){
			error("extra character on line: ')'");
		}
		return f;
	}
}
